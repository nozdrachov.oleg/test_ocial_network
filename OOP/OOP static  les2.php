<?php
class LastUser{
   public static $name;
}

class UserFactory{
    public function create(){
        $user = new User();
        $user->name = rand(1, 100);
        LastUser::$name = $user->name;
        return $user;
    }
}

class User{
    public $name;
}

$UFact = new UserFactory();
$u1 = $UFact->create();
$u2 = $UFact->create();
$u3 = $UFact->create();

echo " $u1->name <br> $u2->name <br> $u3->name";
echo "<hr>";
echo LastUser::$name;
