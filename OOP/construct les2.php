<?php
class Human{
    public $name;
    public $surName;
    public $age;

    public function __construct($name, $sur, $age){
        $this->name = $name;
        $this->surName = $sur;
        $this->age = $age;
    }
}
$human = new Human("Oleg", "Nozdrachov", 24);
echo "$human->surName";