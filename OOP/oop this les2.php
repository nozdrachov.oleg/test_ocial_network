<?php
class Human{
    const name = "Oleg";
    public $surmane;
    public $birYear;
    public $color;

    public function gerFullName(){
        return "{$this->name} {$this->surmane} {$this->getAge()}";
    }

    public function getAge(){
        return date("Y") - $this->birYear;
    }
}

class Collor{
    const BLACK = "#000";
}


$human = new Human();
$human->name = "Oleg";
$human->surmane = "Noz";
$human->birYear = 1998;
$human->color = Collor::BLACK;
echo $human->gerFullName() . " AND $human->color" ;
//echo $human->gerFullName();