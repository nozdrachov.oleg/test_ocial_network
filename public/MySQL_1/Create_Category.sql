create table Category
(
    id   bigint unsigned auto_increment
        primary key,
    name varchar(191) not null
);
