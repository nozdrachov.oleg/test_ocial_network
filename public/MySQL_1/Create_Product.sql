create table Product
(
    id          bigint unsigned auto_increment
        primary key,
    name        varchar(191)    not null,
    price       float           null,
    created_at  timestamp       null,
    updated_at  timestamp       null,
    category_id bigint unsigned null,
    constraint product_category_fk
        foreign key (category_id) references Category (id)
);
